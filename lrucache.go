package lrucache

// Cache is the cache struct itself for the LRU Cache
type Cache struct {
	root     *element
	vals     map[string]*element
	capacity int
	length   int
}

// element is the struct used as part of the double linked list
// for checking order of elements
type element struct {
	val  int
	key  string
	prev *element
	next *element
}

// NewCache creates a new LRU cache
// Must be more than 0 element capacity.
func NewCache(capacity int) Cache {
	if capacity == 0 {
		panic("fail, cant make 0 sized cache")
	}
	root := &element{}
	root.prev = root
	root.next = root
	return Cache{
		vals:     map[string]*element{},
		capacity: capacity,
		root:     root,
	}
}

// Get will return the value for the given key.
// Updates key to be most recently used
func (c *Cache) Get(key string) int {
	//Some default behavior for if the key doesn't exist
	existing, ok := c.vals[key]
	if !ok {
		return -1
	}
	if c.root.next != existing {
		prev := existing.prev
		prev.next = existing.next
		prev.next.prev = prev
		if c.root.prev == existing {
			c.root.prev = prev
		}
		existing.next = c.root.next
		c.root.next.prev = existing
		c.root.next = existing
	}
	return existing.val
}

// Put will put a key/val into cache, updating key to most recently used.
// Evicts oldest element if cache is full
func (c *Cache) Put(key string, value int) {
	existing, ok := c.vals[key]
	if ok {
		existing.val = value
		c.Get(key) // force this key to front
	} else {
		c.length++
		ele := &element{
			val:  value,
			key:  key,
			next: c.root.next,
		}
		c.root.next.prev = ele
		c.root.next = ele
		c.vals[key] = ele
		if c.length > c.capacity {
			c.length--
			remove := c.root.prev
			c.root.prev = c.root.prev.prev
			c.root.prev.next = remove.next
			delete(c.vals, remove.key)
		}
	}
}
