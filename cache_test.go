package lrucache

import (
	"fmt"
	"testing"
)

func TestCache(t *testing.T) {
	cache := NewCache(2)
	cache.Put("1", 10)
	if cache.Get("1") != 10 {
		t.FailNow()
	}
	cache.Put("2", 20)
	if cache.Get("2") != 20 {
		t.FailNow()
	}
	cache.Get("1") // make sure 1 is most recently used
	cache.Put("3", 30)
	if cache.Get("1") != 10 {
		t.Log("1 should still be in the cache")
		t.FailNow()
	}
	if cache.Get("2") != -1 {
		t.Log("2 should have been evicted after putting 3")
		t.FailNow()
	}
	cache.Get("3") // make sure 1 is the least recently used
	cache.Put("4", 40)
	if cache.Get("1") != -1 {
		t.Log("1 did not get evicted when it should.")
		t.FailNow()
	}
	if cache.Get("4") != 40 {
		t.Log("4 not added when it should have been.")
		t.FailNow()
	}
}

func BenchmarkLinkedCache(b *testing.B) {
	nums := []int{10, 100, 1000, 10000}
	keys := make([]string, 100000)
	for i := range keys {
		keys[i] = fmt.Sprintf("%d", i)
	}
	for _, num := range nums {
		b.Run(fmt.Sprintf("PutCacheSize%d", num), func(subb *testing.B) {
			subb.StopTimer()
			cache := NewCache(num)
			for i := 0; i < num; i++ {
				cache.Put(keys[i%100000], i)
			}
			subb.StartTimer()
			for i := 0; i < subb.N; i++ {
				cache.Put(keys[i%100000], i)
			}
		})
	}
}
